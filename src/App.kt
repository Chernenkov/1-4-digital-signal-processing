import java.io.File

fun main(args: Array<String>) {
    val inputFile = File("lena.bmp")

/*4.1*/    ImageOversample(inputFile, 4.0, 4.0, File("lena_4.1.bmp")).doOversamp()
/*4.2*/    ImageOversample(inputFile, 0.3, 0.3, File("lena_4.2.bmp")).doOversamp()
/*4.3*/    ImageOversample(inputFile, 1.5, 0.7, File("lena_4.3.bmp")).doOversamp()
/*4.4*/    ImageOversample(inputFile, 0.7, 1.5, File("lena_4.4.bmp")).doOversamp()

}

